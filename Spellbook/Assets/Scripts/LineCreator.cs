﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCreator : MonoBehaviour {

    [SerializeField] private GameObject line;
    private Vector2 mousePosition;
    int child;



    
    private void Update()
    {

		if (Input.GetKeyDown("space"))
		{
			destroyChildren ();
		}

        if (Input.GetMouseButtonDown(0)) //Or use GetKeyDown with key defined with mouse button
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject newLine = Instantiate(line, mousePosition, Quaternion.Euler(0.0f, 0.0f, 0.0f));

            newLine.transform.parent = gameObject.transform;
        }


        child = transform.childCount;
    }

    public void destroyChildren()
    {
        for( int i = child - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }


}
